# Pet Store API Testing

This is a sample project where we are testing different APIs of sample Pet Store Server using BDD Approach.

API server: https://petstore.swagger.io /

## Installation to setup Environment

Java

Eclipse

Maven

TestNG

Rest Assured Dependencies

Github Account

## Steps to start :

1. Clone / Download the project into your local

2. Open the Command prompt and navigate to project location

3. Execute the following Maven command's

    mvn clean :- To clean the maven repo

    mvn install :- To install the maven requirements

    mvn test :- To execute the test scenarios


## View HTML Report
HTML report will be generated once execution finish
petstore-api-test/target/surefire-reports

Open Index.html in browser to see the reports

## Steps to add New Test Cases
1. Clone / Download the project into your local.

2. Import a project in Eclipse.

3. Navigate to project location

4. Go to petstore-api-test\src\test\java\PetStoreAPITesting Package

5. Add new Class or update existing class.

6. Write/Update Test Cases in Gerkin Format.
