package PetStoreAPITesting;

import static io.restassured.RestAssured.given;

import java.util.HashMap;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;

public class CreateUser {
	
public static HashMap map = new HashMap();
	
	@BeforeClass
	public void postdata()
	{
		
		map.put("id",RestUtils.userID());
	    map.put("username",RestUtils.username());					  
		map.put("firstName",RestUtils.firstName());
		map.put("lastName",RestUtils.lastName());
		map.put("email",RestUtils.email());
		map.put("password",RestUtils.password());
		map.put("phone",RestUtils.phone());
		map.put("userStatus",0);
		
	}
	
	@Test
	public void add_New_User()
	{
		given()
			.contentType("application/json")
			.body(map)
			
		.when()
		
			.post("https://petstore.swagger.io/v2/user")
			
		.then()
		
			.statusCode(200)
			.log().all();
		
		
	}
}
