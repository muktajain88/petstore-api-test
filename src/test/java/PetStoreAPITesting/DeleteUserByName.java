package PetStoreAPITesting;


import java.util.HashMap;

import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import static org.hamcrest.Matchers.*;
import static io.restassured.RestAssured.*;


public class DeleteUserByName {
	
	public static HashMap map = new HashMap();
	String username = RestUtils.username();
	String id = RestUtils.userID();
	
	@BeforeClass
	public void postdata()
	{
		
		map.put("id",id);
	    map.put("username",username);					  
		map.put("firstName",RestUtils.firstName());
		map.put("lastName",RestUtils.lastName());
		map.put("email",RestUtils.email());
		map.put("password",RestUtils.password());
		map.put("phone",RestUtils.phone());
		map.put("userStatus",0);
		
		RestAssured.baseURI ="https://petstore.swagger.io/v2/user";
		given()
			.contentType("application/json")
			.body(map)
			
		.when()
			.post()
		.then()
			.statusCode(200);
	
	}
	
	
	@Test(priority=1)
	/*TC(Negative- To delete order by order id */
	
	public void delete_user_by_name()
	{
		RestAssured.baseURI ="https://petstore.swagger.io/v2/user";
		RestAssured.basePath ="/"+username;
			
		
		Response response= 
		given()
		
		.when()
	
			.delete()
		
		.then()
	
			.statusCode(200)
			.statusLine("HTTP/1.1 200 OK")
			.log().all()
			.extract().response();
	
	String jsonAsString = response.asString();
	Assert.assertEquals(jsonAsString.contains((username)),true);
	}
	
	
	@Test(priority=2)
	/*TC(Negative- To delete order when order id donot exist*/
	public void delete_user_not_exist()
	{
		RestAssured.baseURI ="https://petstore.swagger.io/v2/user";
		RestAssured.basePath ="/"+username;
						
		given()
		
		.when()
	
			.delete()
		
		.then()
	
			.statusCode(404)
			.log().all();
			}
	

	
}