package PetStoreAPITesting;

	import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;

import static org.hamcrest.Matchers.*;

import java.util.HashMap;

import static io.restassured.RestAssured.*;



public class FindByUserByName {
	
public static HashMap map = new HashMap();
String username = RestUtils.username();
	
	@BeforeClass
	public void postdata()
	{
		
		map.put("username",username);					  
	}

		@Test(priority=1)
	    /*TC1: To find the user by Name*/
	    public void findUserByName()
	    {
			RestAssured.baseURI ="https://petstore.swagger.io/v2/user/string";
			RestAssured.basePath ="";
						
	        given()
	        .when()
	        	.get()
	        .then()
	        	.statusCode(200)
	        	.log().all();
	    }
		
	     @Test(priority=1)
		 /*TC2(Negative): To find the pet by Name(not exist)*/
	    public void findUserByName_neg()
	    {
	    	 RestAssured.baseURI ="https://petstore.swagger.io/v2/user";
	    	 RestAssured.basePath ="/0"+username;
				
	        given()
	        .when()
	        	.get()
	        .then()
	        	.statusCode(404)
	        	.log().all();
	    }
	     
	}

