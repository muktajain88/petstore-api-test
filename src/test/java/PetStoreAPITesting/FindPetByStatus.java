package PetStoreAPITesting;

import org.testng.annotations.Test;
import static org.hamcrest.Matchers.*;
import static io.restassured.RestAssured.*;

public class FindPetByStatus {


	@Test(priority=1)
    /*TC1: To find the pet by status- "Available"*/
    public void findPetByStaus_available()
    {
        given()
        .when()
        .get("https://petstore.swagger.io/v2/pet/findByStatus?status=available")
        .then()
        .statusCode(200);
        //.log().all();
    }
	
	@Test(priority=2)
    /*TC2: To find the pet by status- "pending"*/
    public void findPetByStaus_pending()
    {
        given()
        .when()
        .get("https://petstore.swagger.io/v2/pet/findByStatus?status=pending")
        .then()
        .statusCode(200);
        //.log().all();
    }
	
	
	@Test(priority=3)
    /*TC3: To find the pet by status- "sold"*/
    public void findPetByStaus_sold()
    {
        given()
        .when()
        .get("https://petstore.swagger.io/v2/pet/findByStatus?status=sold")
        .then()
        .statusCode(200);
        //.log().all();
    }
	
	@Test(priority=4)
    /*TC4(Negative): To find the pet by status- "other"*/
    public void findPetByStaus_other()
    {
        given()
        .when()
        .get("https://petstore.swagger.io/v2/pet/findByStatus?status=other")
        .then()
        .statusCode(200)
        .log().all();
    }
	
	
}

