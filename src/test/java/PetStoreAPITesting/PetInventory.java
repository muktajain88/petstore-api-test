package PetStoreAPITesting;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import org.testng.annotations.Test;

public class PetInventory {
	
	@Test
    /*TC1: To know the status of Pet Inventory*/
    public void pet_Inventory()
    {
        given()
        .when()
        .get("https://petstore.swagger.io/v2/store/inventory")
        .then()
        .statusCode(200)
        .header("content-type","application/json")
        .log().all();
    }
    
}
