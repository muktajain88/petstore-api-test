package PetStoreAPITesting;

import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;

public class RestUtils {
	
	public static int getID()
	{
	Random rand = new Random();
	int upperbound = 25;
	int id = rand.nextInt(upperbound);
	return(id);
}

	public static String getPetID()
	{
		String generatedString= RandomStringUtils.randomNumeric(6);
		return(generatedString);
}
	
	public static String getcategory()
	{
		String generatedString= RandomStringUtils.randomAlphabetic(1);
		return("012"+ generatedString);
}
	
	public static String quantity()
	{
		String generatedString= RandomStringUtils.randomNumeric(1);
		return(generatedString);
}
	
	public static String petName()
	{
	String generatedString= RandomStringUtils.randomAlphabetic(1);
			return("Doggie"+generatedString);
	}
	
	public static String petPhotoUrls()
	{
	String generatedString= RandomStringUtils.randomAlphabetic(1);
			return("image"+generatedString);
	}
	
	
	public static String userID()
	{
	String generatedString= RandomStringUtils.randomNumeric(1);
			return(generatedString);
	}
	
	
	public static String username()
	{
	String generatedString= RandomStringUtils.randomAlphabetic(2);
			return("Mario"+generatedString);
	}
	
	public static String firstName()
	{
	String generatedString= RandomStringUtils.randomAlphabetic(2);
			return("Mario"+generatedString);
	}
	
	public static String lastName()
	{
	String generatedString= RandomStringUtils.randomAlphabetic(2);
			return("Test"+generatedString);
	}
	
	public static String email()
	{
	String generatedString= RandomStringUtils.randomAlphabetic(6);
			return(generatedString+"@gmail.com");
	}
	
	public static String password()
	{
	String generatedString= RandomStringUtils.randomAlphabetic(6);
			return(generatedString);
	}
	
	public static String phone()
	{
	String generatedString= RandomStringUtils.randomNumeric(10);
			return(generatedString);
	}
	
}

	