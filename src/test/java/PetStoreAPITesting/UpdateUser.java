package PetStoreAPITesting;

import java.util.HashMap;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;

import static org.hamcrest.Matchers.*;
import static io.restassured.RestAssured.*;



public class UpdateUser {
	
	public static HashMap map = new HashMap();
	String username = RestUtils.username();
 
	@BeforeClass
	public void postdata()
	{
		
		map.put("id",RestUtils.userID());
	    map.put("username",RestUtils.username());					  
		map.put("firstName",RestUtils.firstName());
		map.put("lastName",RestUtils.lastName());
		map.put("email",RestUtils.email());
		map.put("password",RestUtils.password());
		map.put("phone",RestUtils.phone());
		map.put("userStatus",0);
		
	}
	@Test(priority=1)
	public void Add_user_to_update()
	{
		
		given()
			.contentType("application/json")
			.body(map)
		
		.when()
	
			.post("https://petstore.swagger.io/v2/user")
		
		.then()
	
			.statusCode(200);
	}
	
	@Test(priority=2)
	/* TC1- To update the info of user*/
	public void update_user_Info()
	{
	
		RestAssured.baseURI ="https://petstore.swagger.io/v2/user";
		RestAssured.basePath ="/"+username;
		
		given()
			.contentType("application/json")
			.body(map)
			
		.when()
		
			.put()
			
		.then()
		
			.statusCode(200)
			.log().all();
		
		
	}
	
	
}