package PetStoreAPITesting;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.util.HashMap;

import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;


public class Upload_Pet_Image {
	
	@BeforeClass
	public void image()
	{	
		File file = new File("pathname","C:\\project\\image1.jpg");
		
	}
	
	@Test
	public void upload_file()
	{
		Response response=
		given()
			.multiPart("file","application/json")
		.when()
			.post("https://petstore.swagger.io/v2/pet/1/uploadImage")
		.then()
			.statusCode(200)
			.log().all()
			.extract().response();
		
		String jsonAsString = response.asString();
		Assert.assertEquals(jsonAsString.contains("File uploaded"),true);
		
	
	}
	

}
