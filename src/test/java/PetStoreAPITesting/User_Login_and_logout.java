package PetStoreAPITesting;

import static io.restassured.RestAssured.given;

import java.util.HashMap;

import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class User_Login_and_logout {
	
public static HashMap map = new HashMap();
	
	@BeforeClass
	public void postdata()
	{
		String id = RestUtils.userID();
		map.put("id",id);
	    map.put("username","TestName123");					  
		map.put("firstName",RestUtils.firstName());
		map.put("lastName",RestUtils.lastName());
		map.put("email",RestUtils.email());
		map.put("password","Test123");
		map.put("phone",RestUtils.phone());
		map.put("userStatus",id);
		
	}
	
	@Test(priority=1)
	public void add_New_User_for_auth()
	{
				
		given()
			.contentType("application/json")
			.body(map)
			
		.when()
		
			.post("https://petstore.swagger.io/v2/user")
			
		.then()
		
			.statusCode(200)
			.log().all();
	}
	
	@Test(priority=2)
	/*User Log in*/
	public void Valid_User_Login()
	{
		Response response= 
				
		given()
			.auth()
			.preemptive()
			.basic("TestName", "Test123")
			
		.when()
		
			.get("https://petstore.swagger.io/v2/user/login")
			
		.then()
			.statusCode(200)
			.log().all()
			.extract().response();
		
		String jsonAsString = response.asString();
		Assert.assertEquals(jsonAsString.contains("logged in user session"),true);
		
		
		
	}


	@Test(priority=3)
	/*User Log in*/
	public void User_Logout()
	{
		Response response= 
		given()
			
		.when()
		
			.get("https://petstore.swagger.io/v2/user/logout")
			
		.then()
			.statusCode(200)
			.log().all()
			.extract().response();
		
		String jsonAsString = response.asString();
		Assert.assertEquals(jsonAsString.contains("ok"),true);
		
		
		
	}
}

