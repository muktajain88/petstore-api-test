package PetStoreAPITesting;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import java.util.HashMap;

import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class order_api_test_cases {
	
	public static HashMap map = new HashMap();
	int id = RestUtils.getID();
	
	@BeforeClass
	public void postdata()
	{
		map.put("id",id);
		map.put("petId",RestUtils.getPetID());
		map.put("quantity",RestUtils.quantity());
		map.put("shipDate","2020-10-09");
		map.put("status","placed");
		map.put("complete","false");
		
	}
	
	@Test(priority=1)
	/* TC1- To place order*/
	public void place_Order_For_Pet()
	{
		RestAssured.baseURI ="https://petstore.swagger.io/v2/store";
		RestAssured.basePath ="/order";
	
		given()
			.contentType("application/json")
			.body(map)
			
		.when()
		
			.post()
			
		.then()
		
			.statusCode(200)
			.and()
			.body("status",equalTo("placed"))
			.log().all();
	}
	
	@Test(priority=2)
	 /*TC1: To know the status of Order by ID exist*/
    public void find_orderStatusbyID()
    {
		RestAssured.baseURI ="https://petstore.swagger.io/v2/store";
		RestAssured.basePath ="/order/"+id;
			
        given()        
        .when()
        	.get()
        .then()
        	.statusCode(200)
        	.assertThat().body("status",equalTo("placed"))
        	.header("content-type","application/json")
        	.log().all();
    }
   
	@Test(priority=3)
	/*TC2(To delete order by order id */
	
	public void delete_by_order_id()
	{
		RestAssured.baseURI ="https://petstore.swagger.io/v2/store";
		RestAssured.basePath ="/order/"+id;
		
				
		Response response= 
		given()

		.when()
	
			.delete()
		
		.then()
	
			.statusCode(200)
			.statusLine("HTTP/1.1 200 OK")
			.log().all()
			.extract().response();
	
	String jsonAsString = response.asString();
	System.out.println(jsonAsString);
	Assert.assertEquals(jsonAsString.contains(Integer.toString(id)), true);
	}
	
	
	@Test(priority=4)
	/*TC3(Negative- To delete order when order id donot exist*/
	public void delete_by_order_id_not_exist()
	{
		RestAssured.baseURI ="https://petstore.swagger.io/v2/store";
		RestAssured.basePath ="/order/"+id;
						
		given()
		
		.when()
	
			.delete()
		
		.then()
	
			.statusCode(404);
			}
	
	@Test(priority=5)
	/*TC1: To know the status of Order by ID not exist*/
	public void find_orderStatusbyID_not_exist()
	{
		RestAssured.baseURI ="https://petstore.swagger.io/v2/store";
		RestAssured.basePath ="/order/"+id;
			
	   given()        
	   .when()
	   	.get()
	   .then()
	   	.statusCode(404);
	}


}
