package PetStoreAPITesting;

import static io.restassured.RestAssured.given;

import java.util.HashMap;

import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class pet_api_by_id_test_cases {
	
public static HashMap map = new HashMap();

String id = RestUtils.getPetID();
	
	@BeforeClass
	public void postdata()
	{
		
		map.put("id",id);
	    map.put("category.id",1);					  
		map.put("name",RestUtils.petName());
		map.put("category.name",RestUtils.petName());
		map.put("tags[0].name",RestUtils.petName());
		map.put("photoUrls[0]",RestUtils.petPhotoUrls());
		map.put("tags[0].id",1);
		map.put("status","available");
			
			}
	
	@Test(priority=1)
	public void addNewPetToStore()
	{
		RestAssured.baseURI ="https://petstore.swagger.io/v2/pet";
		RestAssured.basePath ="";
				
		given()
			.contentType("application/json")
			.body(map)
			
		.when()
		
			.post()
			
		.then()
		
			.statusCode(200)
			.log().all();
		
		
	}	

	
	@Test(priority=2)
    /*TC1: To find the pet by Pet ID*/
    public void findPetByID()
    {
		RestAssured.baseURI ="https://petstore.swagger.io/v2/pet";
		RestAssured.basePath ="/"+id;
		
		
        given()
        .when()
        .get()
        .then()
        .statusCode(200)
        .log().all();
    }
	
	

	@Test(priority=4)
	/*TC4: To delete order by order id */
	
	public void delete_pet_by_id()
	{
		
				
		RestAssured.baseURI ="https://petstore.swagger.io/v2/pet";
		RestAssured.basePath ="/"+id;
		
		Response response= 
		given()
		
		.when()
	
			.delete()
		
		.then()
	
			.statusCode(200)
			.statusLine("HTTP/1.1 200 OK")
			.log().all()
			.extract().response();
	
	String jsonAsString = response.asString();
	Assert.assertEquals(jsonAsString.contains(id),true);
	}
	
	@Test(priority=5)
	 /*TC5(Negative): To find the pet by Pet ID(not exist)*/
   public void findPetByID_neg()
   {
		RestAssured.baseURI ="https://petstore.swagger.io/v2/pet";
		RestAssured.basePath ="/"+id;
				
       given()
       .when()
       .get()
       .then()
       .statusCode(404)
       .log().all();
   }
	
	@Test(priority=6)
	/*TC6(Negative- To delete order when order id do not exist*/
	public void delete_pet_by_id_not_exist()
	{
		
				
		RestAssured.baseURI ="https://petstore.swagger.io/v2/pet";
		RestAssured.basePath ="/"+id;
		
		given()
		
		.when()
	
			.delete()
		
		.then()
	
			.statusCode(404)
			.log().all();
			}
	@Test(priority=7)
	/* TC7(Negative)- To update the info when pet not exist in store*/
	public void update_Non_Exist_Pet_Info()
	{
		
		RestAssured.baseURI ="https://petstore.swagger.io/v2/pet";
		RestAssured.basePath ="/"+id;
		
		
		given()
			.contentType("application/json")
			.body(map)
			
		.when()
		
		.put()
		
		.then()
		
			.statusCode(405)
			.log().all();
		
		
	}



	}
